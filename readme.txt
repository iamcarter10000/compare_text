﻿（1）Compare_Text和Compare_Text.exe是基于 对话框 的程序，前者是linux下运行的，后者是windows下运行的。两个文本框中各输入一个字符串，点击“compare”按钮执行比较操作。（文件完整路径（比较文本文件差异），可见字符串（比较字符串差异））。
（2）CompareText和CompareText.exe是基于 控制台 的程序，打开控制台程序进入当前路径，运行可执行此文件，后面的两个参数同上。（linux下更改权限后：./CompareText 参数1 参数2 ; windows下：CompareText.exe 参数1 参数2）
（3）如果linux下运行对话框程序时，提示找不到文件，进入当前路径后，操作如下：
cp -ra libwx_gtk2u-3.1.so.0.0.0 /usr/lib/libwx_gtk2u-3.1.so.0 && cp -ra libpng12.so.0 /usr/lib/libpng12.so.0 && ldconfig